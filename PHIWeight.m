//
//  PHIWeight.m
//  LittleGym
//
//  Created by Richard George on 20/12/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "PHIWeight.h"

@implementation PHIWeight
+ (NSString *)prettyPrintPoundsOrKilosFromFloatKilos:(float)kilograms asMetric:(BOOL) asMetric {
    
    NSString *returnString=nil;
    if(asMetric) {
        returnString = [NSString stringWithFormat:@"%.1f kg", kilograms];
    } else {
        returnString = [NSString stringWithFormat:@"%.1f lb", kilograms * PHI_WEIGHT_POUNDS_IN_A_KILO];
    }
    
    return returnString;
}

@end
