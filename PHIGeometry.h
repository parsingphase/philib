//
//  PHIGeometry.h
//  Nelson
//
//  Created by Richard George on 05/01/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PHIGeometry : NSObject

+ (double) degreesToRadians: (double) degrees;
+ (double) radiansToDegrees:(double) radians;

@end
