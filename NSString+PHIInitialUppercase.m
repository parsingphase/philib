//
//  NSString+PHIInitialUppercase.m
//  LittleGym
//
//  Created by Richard George on 14/12/2011.
//  Copyright (c) 2011 Richard George. All rights reserved.
//

#import "NSString+PHIInitialUppercase.h"

@implementation NSString (PHIInitialUppercase)

- (NSString *)stringWithInitialUppercase {
    return [self stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[self substringToIndex:1] uppercaseString]];
}

@end
