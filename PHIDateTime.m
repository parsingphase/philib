//
//  PHIDateTime.m
//  LittleGym
//
//  Created by Richard George on 03/12/2011.
//  Copyright (c) 2011 Richard George. All rights reserved.
//

#import "PHIDateTime.h"

@implementation PHIDateTime

+ (NSString *)humanHMSStringFromNSTimeInterval:(NSTimeInterval) interval
{
    NSMutableString *humanString = [[NSMutableString alloc] init];
    if (interval>3600.0) {
        [humanString appendFormat:@"%.0f hours ", floor(interval/(NSTimeInterval)3600.0) ];
        interval = fmod(interval, (NSTimeInterval) 3600.0); // chop out the whole hours
    }
    
    if (interval>60.0) {
        [humanString appendFormat:@"%.0f minutes ", floor(interval/(NSTimeInterval)60.0) ];
        interval = fmod(interval, (NSTimeInterval) 60.0); // chop out the whole hours
    }
    
    if(interval>0.0001) { 
        //sanity floor
        [humanString appendFormat:@"%.2f seconds ", interval];

    }
    
    //retun immutable, trimmed;
    NSString *returnString = [humanString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    [humanString release];
    return returnString;
}

//TODO refactor these to use one formatting function
+ (NSString *)shortHMSStringFromNSTimeInterval: (NSTimeInterval) interval {
    BOOL showsHours = NO;
    
    NSMutableString *humanString = [[NSMutableString alloc] init];
    if (interval>3600.0) {
        [humanString appendFormat:@"%.0f:", floor(interval/(NSTimeInterval)3600.0) ];
        interval = fmod(interval, (NSTimeInterval) 3600.0); // chop out the whole hours
        showsHours = YES;
    }
    
    if (showsHours || (interval>60.0)) {
        [humanString appendFormat:@"%02.0f:", floor(interval/(NSTimeInterval)60.0) ];
        interval = fmod(interval, (NSTimeInterval) 60.0); // chop out the whole hours
        [humanString appendFormat:@"%02.0f", interval]; // always display seconds if anything else displays.
    }
        
    //retun immutable, trimmed;
    NSString *returnString = [humanString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    [humanString release];
    return returnString;
}


@end
