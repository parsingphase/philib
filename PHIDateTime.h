//
//  PHIDateTime.h
//  LittleGym
//
//  Created by Richard George on 03/12/2011.
//  Copyright (c) 2011 Richard George. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PHIDateTime : NSObject

+ (NSString *)humanHMSStringFromNSTimeInterval:(NSTimeInterval) interval;
+ (NSString *)shortHMSStringFromNSTimeInterval:(NSTimeInterval) interval;

@end
