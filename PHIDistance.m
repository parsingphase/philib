//
//  PHIDistance.m
//  LittleGym
//
//  Created by Richard George on 03/12/2011.
//  Copyright (c) 2011 Richard George. All rights reserved.
//

#import "PHIDistance.h"

@implementation PHIDistance

+ (NSString *)prettyPrintKilometersMetresFromDoubleMetres:(double) distance
{
    NSMutableString *distanceString, *distanceString2, *distanceString3;
    NSString *returnString;
    if(distance>=1000.0) {
        distanceString = [NSMutableString stringWithFormat:@"%.3f",distance/1000.0];
        
        //Remove trailing 0s', . if it gets that far
        distanceString2 = [[distanceString stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"0"]] mutableCopy]; 
        
        //only trim 0s BEFORE you trim .s!
        distanceString3 = [[distanceString2 stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"."]] mutableCopy]; 
        
        [distanceString3 appendString:@" km"];
        //        distanceString = 
        returnString = [NSString stringWithString:distanceString3];
        [distanceString2 release];
        [distanceString3 release];
        // Analyzer says so. Am confused.
    } else {
        distanceString = [NSString stringWithFormat:@"%.0f m",distance];
        returnString = [NSString stringWithString:distanceString];
    }

    return returnString;
}

+ (NSString *)prettyPrintMilesYardsFromDoubleMetres:(double) metres
{
    //1760 yards in a mile
    NSMutableString *distanceString, *distanceString2, *distanceString3;
    NSString *returnString;
    
    double distance = metres * 1.0936133;
    if(distance>=1760) {
        distanceString = [NSMutableString stringWithFormat:@"%.3f",distance/1760.0];
        
        //Remove trailing 0s', . if it gets that far
        distanceString2 = [[distanceString stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"0"]] mutableCopy]; 
        
        //only trim 0s BEFORE you trim .s!
        distanceString3 = [[distanceString2 stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"."]] mutableCopy]; 
        
        if([distanceString3 isEqualToString:@"1"]) {
            [distanceString3 appendString:@" mile"]; 
        } else {
            [distanceString3 appendString:@" miles"]; 
        }
        //        distanceString = 
        returnString = [NSString stringWithString:distanceString3];
        [distanceString2 release];
        [distanceString3 release];
        // Analyzer says so. Am confused.
    } else {
        distanceString = [NSString stringWithFormat:@"%.0f yards",distance];
        returnString = [NSString stringWithString:distanceString];
    }
    
    return returnString;
}

+ (NSString *)prettyPrintMilesOrKilometersFromDoubleMetres:(double)metres asMetric:(BOOL) asMetric
{
    if(asMetric) {
        return [self prettyPrintKilometersMetresFromDoubleMetres:metres];
    } else {
        return [self prettyPrintMilesYardsFromDoubleMetres:metres];
    }
}

@end
