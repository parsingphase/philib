//
//  PHIGeolocation.m
//  Nelson
//
//  Created by Richard George on 05/01/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PHIGeolocation.h"

@implementation PHIGeolocation

+ (CLLocationDirection) greatCircleBearingFromLocation:(CLLocationCoordinate2D)fromLocation toLocation: (CLLocationCoordinate2D)toLocation
{
    CLLocationDirection direction;
    //cartesian for now, we can play with haversine later
    //       d-lon
    //       |--- t
    // d-lat |  /
    //       | /
    //      f|a
    
    // tan(a) = opp/adj = d-lon/d-lat
    //The atan2(y,x) function computes the principal value of the arc tangent of y/x
    //    direction = atan2(toLocation.longitude-fromLocation.longitude, toLocation.latitude-fromLocation.latitude) * /*radians to degrees*/ 180/M_PI ;
    
    // OK, cartesian is bollocks in spherical geometry. Scrap that.
    
    //http://www.movable-type.co.uk/scripts/latlong.html
    /* Formula: 	θ = 	atan2( 	sin(Δlong).cos(lat2),
     cos(lat1).sin(lat2) − sin(lat1).cos(lat2).cos(Δlong) )
     */
    
    CLLocationDegrees deltaLongitude = [PHIGeometry degreesToRadians:(double) (toLocation.longitude-fromLocation.longitude)];
    
    direction = atan2(sin(deltaLongitude)*cos([PHIGeometry degreesToRadians:(double)toLocation.latitude]),
                      (cos([PHIGeometry degreesToRadians:(double)fromLocation.latitude]) *
                       sin([PHIGeometry degreesToRadians:(double)toLocation.latitude])) -
                      (cos([PHIGeometry degreesToRadians:(double)toLocation.latitude]) *
                       sin([PHIGeometry degreesToRadians:(double)fromLocation.latitude]) *
                       cos(deltaLongitude))
                      );
    
    return direction;
}

/*
 Relative heading:
 
 CLLocationDirection deviceBearingInRadians = M_PI*heading.trueHeading/180;
 self.pointerImage.transform = CGAffineTransformMakeRotation(2*M_PI + trafalgarBearing - deviceBearingInRadians);
 
 */


+ (double) greatCircleDistanceFromLocation:(CLLocationCoordinate2D)fromLocation toLocation: (CLLocationCoordinate2D)toLocation onSphereOfRadiusMetres: (double) radius
{
    
//    again from http://www.movable-type.co.uk/scripts/latlong.html
    CLLocationDegrees deltaLongitude = [PHIGeometry degreesToRadians:(double) (toLocation.longitude-fromLocation.longitude)];
    
    CLLocationDegrees deltaLatitude = [PHIGeometry degreesToRadians:(double) (toLocation.latitude-fromLocation.latitude)];
    
    double a = (sin(deltaLatitude/2) * sin(deltaLatitude/2)) +
        (sin(deltaLongitude/2) * sin(deltaLongitude/2) * 
        cos([PHIGeometry degreesToRadians:(double) toLocation.latitude]) *
        cos([PHIGeometry degreesToRadians:(double) fromLocation.latitude])); 
    
    double c = 2 * atan2(sqrt(a), sqrt(1-a)); 
    double distance = radius * c;
    
//    PSLogDebug(@"Distance from %f,%f to %f,%f is %fm", fromLocation.latitude,fromLocation.longitude, toLocation.latitude, toLocation.longitude, distance);
    return distance;
}

+ (double) earthMeanRadiusMetres {
    return (double)6371000;
}

@end
