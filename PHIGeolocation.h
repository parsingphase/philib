//
//  PHIGeolocation.h
//  Nelson
//
//  Created by Richard George on 05/01/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#import "PHIGeometry.h"

@interface PHIGeolocation : NSObject

+ (CLLocationDirection) greatCircleBearingFromLocation:(CLLocationCoordinate2D)fromLocation toLocation: (CLLocationCoordinate2D)toLocation;

+ (double) greatCircleDistanceFromLocation:(CLLocationCoordinate2D)fromLocation toLocation: (CLLocationCoordinate2D)toLocation onSphereOfRadiusMetres: (double) radius;

+ (double) earthMeanRadiusMetres;

@end
