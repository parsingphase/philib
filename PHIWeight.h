//
//  PHIWeight.h
//  LittleGym
//
//  Created by Richard George on 20/12/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#define PHI_WEIGHT_POUNDS_IN_A_KILO (2.20462262)

@interface PHIWeight : NSObject
+ (NSString *)prettyPrintPoundsOrKilosFromFloatKilos:(float)kilograms asMetric:(BOOL) asMetric;
@end
