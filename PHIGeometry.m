//
//  PHIGeometry.m
//  Nelson
//
//  Created by Richard George on 05/01/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PHIGeometry.h"

@implementation PHIGeometry

#pragma mark - location calculations

+ (double) degreesToRadians:(double) degrees
{
    return degrees * M_PI / 180;
};

+ (double) radiansToDegrees:(double) radians
{
    return radians * 180 / M_PI;
};

@end
