//
//  NSString+PHIInitialUppercase.h
//  LittleGym
//
//  Created by Richard George on 14/12/2011.
//  Copyright (c) 2011 Richard George. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (PHIInitialUppercase)

///Return copy of string with its first letter uppercased
- (NSString *)stringWithInitialUppercase;
@end
