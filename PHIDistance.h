//
//  PHIDistance.h
//  LittleGym
//
//  Created by Richard George on 03/12/2011.
//  Copyright (c) 2011 Richard George. All rights reserved.
//

#import <Foundation/Foundation.h>
#define PHI_DISTANCE_KILOMETERS_IN_A_MILE (1.609344)
#define PHI_DISTANCE_YARDS_IN_A_MILE (1760.0)

@interface PHIDistance : NSObject
+ (NSString *)prettyPrintKilometersMetresFromDoubleMetres:(double) distance;
+ (NSString *)prettyPrintMilesYardsFromDoubleMetres:(double) metres;
+ (NSString *)prettyPrintMilesOrKilometersFromDoubleMetres:(double)metres asMetric:(BOOL) asMetric;
@end
